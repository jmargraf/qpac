
import matplotlib.pyplot as plt
from qpac.kernel import *
from qpac.utils import *
from qpac.kQEq import kernel_qeq
from qpac.descriptors import dscribeSOAP

from ase.units import Hartree
from ase.io import read, write

import numpy as np
atom_energy = {"Zn": -49117.02929728, "O": -2041.3604}

hard_lib = {"O": 1*Hartree, "Zn": 1*Hartree}



SOAPclass = dscribeSOAP(nmax = 6,
                   lmax =5 ,
                    rcut = 3.0,
                    sigma = 3.0/8,
                    species = ["Zn","O"],
                    periodic = False)



SOAP_Kernel = SOAPKernel(multi_SOAP=False,
                         perEl = False,
                         Descriptor=SOAPclass)


# Create an instance of the kQEq class

my_kqeq = kernel_qeq(Kernel=SOAP_Kernel,
                     scale_atsize=1.0/np.sqrt(2),
                     radius_type="qeq",
                     hard_lib = hard_lib)

my_kqeq.load_kQEq()

test_set = read("particles5.xyz@:", format="extxyz")[:200] 
E_test = []
F_test = []
for a in test_set:
    res = my_kqeq.calculateEnergy(a)
    E_test.append(res/len(a))
ref_en_test = get_energies_perAtom(mols=test_set,atom_energy = atom_energy)


plt.figure(figsize=(10,8), dpi=100)
plt.plot(ref_en_test,ref_en_test,alpha = 0.5,color="black")
plt.scatter(E_test,ref_en_test,alpha = 0.5)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.show()

