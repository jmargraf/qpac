import sys
sys.path.append("/home/mvondrak/work/PaperQPAC2/qpac/")
from qpac.descriptors import quipSOAP
from qpac.kernel import *
from qpac.utils import *
from qpac.kQEq import kernel_qeq
from qpac.GAP import GAP
from qpac.MLmodel import MLmodel
from qpac.utils_plots import plot_basics
from ase.units import Hartree
from ase.io import read, write
import numpy as np
import random

aEs = {"H":-13.641637275,"O":-2043.796858679}

molsNeut = read("xtbneut100.xyz@:",format="extxyz")
molsPos  = read("xtbpos100.xyz@:",format="extxyz")
molsNeg = read("xtbneg100.xyz@:",format="extxyz")


# In[29]:


train_set = molsNeut[:10]


# In[30]:

soap_desctipor1 = quipSOAP(STRsoap="soap cutoff=3.0 l_max=5 n_max=6 atom_sigma=0.5 normalize=T n_species=2 species_Z={1 8} n_Z=2 Z={1 8}", species=["H","O"])
soap_desctipor2 = quipSOAP(STRsoap="soap cutoff=2.4 l_max=6 n_max=8 atom_sigma=0.4 normalize=T n_species=2 species_Z={1 8} n_Z=2 Z={1 8}", species=["H","O"])
soap_desctipor3 = quipSOAP(STRsoap="soap cutoff=5.0 l_max=3 n_max=8 atom_sigma=1.2 normalize=T n_species=2 species_Z={1 8} n_Z=2 Z={1 8}", species=["H","O"])

qpac_kernel = qpacKernel(Descriptors=[soap_desctipor1,soap_desctipor3],#,soap_desctipor3],
                         training_set=train_set,
                         training_set_charges=[a.info["total_charge"] for a in train_set],
                         sparse = True,
                         perEl = True,      
                         sparse_method = "CUR",
                         sparse_count=500)
qpac_gap = GAP(Kernel=qpac_kernel)
qpac_kqeq = kernel_qeq(Kernel=qpac_kernel,
                     scale_atsize=1/np.sqrt(2.0),
                     radius_type="qeq")

qpac_ML = MLmodel(kQEqclass = qpac_kqeq,
                    GAPclass = qpac_gap)
qpac_ML.train( 
              targets = ["energy","forces"], 
              target_sigmas = [0.0000001,0.001], 
              charge_keyword='dft_hirshfeld',
              max_iter = 50,
              atom_energy=aEs,
              n_bad_iters = 20,
              scc_target=False,
              scc_sigma=0.1,
              energy_keyword="dft_energy",
              forces_keyword="dft_forces",
              verbose=True)


# In[37]:


E_test = []
q_test = []
F_test = []
for a in train_set:
    temp = qpac_ML.calculate(a,ext_field=a.info["ext_field"],charge=a.info["total_charge"])
    E_test.append(temp["energy"]/len(a))
    q_test.extend(temp["charges"])
    F_test.extend(temp["forces"].flatten())

ref_en_test  = get_energies_perAtom_eV(mols=train_set,atom_energy = aEs,energy_keyword="dft_energy")
ref_q_test = get_charges(mols=train_set, charge_keyword="dft_hirshfeld")
ref_f_test = get_forces_eV(mols=train_set,forces_keyword="dft_forces")


# In[39]:


plot_basics(ref=ref_en_test,kqeq = E_test,preset="energy")
plot_basics(ref=ref_q_test,kqeq = q_test,preset="charges")
plot_basics(ref=ref_f_test,kqeq = F_test,preset="forces")


# In[ ]:




