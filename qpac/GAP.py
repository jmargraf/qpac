import numpy as np

from ase.io import *
from ase.data import covalent_radii
from ase.units import Bohr, Hartree
from ase import Atoms
import random
# from quippy.descriptors import Descriptor
from qpac.data import atomic_numbers
from qpac.utils import *
from qpac.kQEq import kernel_qeq

from scipy.optimize import minimize
class GAP():
    """Class for Kernel Charge Equilibration Models

    Parameters
    ----------
        Kernel: obj
            Kernel object with the function kernel_matrix
        scale_atsize: float
            Scaling factor for covalent radii, used to define atom sizes
        radius_type: string
            'rcov' or 'qeq' (default)
        sparse: bool
            Setting training to use full or sparse training set
        sparse_count: int
            Number of training points used for sparsed training
        sparse_method: string
            Method used to specify representative set "FPS", or "CUR" (default) 
    """

    def __init__(self, Kernel=None,Kmm_jiter = 1.0e-10):
        
        if Kernel == None:
            print("Specify kernel!")
            exit()
        # inputs and defaults
        self.Kernel       = Kernel
        self.weights      = None
        if self.Kernel.training_set[0] is not None:
            self.elements = self.Kernel.elements
            self.K_nms, K_mms = self.Kernel.kernel_matrices(kerneltype='training')
            for K_mm in K_mms:
                K_mm += np.eye(K_mm.shape[0])*Kmm_jiter
            self.K_mms = K_mms

    def save_GAP(self, nameW="GAPWeights",nameS="GAPSOAP",nameEl="GAPEl"):
        """Saves trained model weights, SOAP vector of the representative set, and array of elements corresponding to representatve set.

        Parameters
        ----------
            nameW : string
                Name of the file of weights
            nameS : string
                Name of the file of SOAP vectors
            nameEl : string
                Name of the file of elements
        """
        print("Saving GAP model")
        np.save(nameW, self.weights)
        for iddesc in range(len(self.Kernel.Descriptors)):
            np.save(f"{nameS}_{iddesc}" , self.Kernel.representing_set_descriptors[iddesc])
            np.save(f"{nameEl}_{iddesc}", self.Kernel.representing_set_elements[iddesc])
    

    def load_GAP(self, nameW="GAPWeights",nameS="GAPSOAP",nameEl="GAPEl"):
        """Loads trained model weights, SOAP vector of the representative set, and array of elements corresponding to representatve set.

        Parameters
        ----------
            nameW : string
                Name of the file of weights
            nameS : string
                Name of the file of SOAP vectors
            nameEl : string
                Name of the file of elements
        """
        print("Loading gap model")
        self.weights = np.load(f"{nameW}.npy")
        self.Kernel.representing_set_descriptors = []
        self.Kernel.representing_set_elements = []
        for iddesc in range(len(self.Kernel.Descriptors)):
            self.Kernel.representing_set_descriptors.append(np.load(f"{nameS}_{iddesc}.npy"))
            self.Kernel.representing_set_elements.append(np.load(f"{nameEl}_{iddesc}.npy").squeeze())
        self.Kernel.repres_descs = []
        for iddes in range(len(self.Kernel.Descriptors)):
            repres_descs_temp = {}
            ldesc = len(self.Kernel.representing_set_descriptors[iddes][0])
            for el in self.Kernel.elements:
                repres_descs_temp[el] = []
                for countRepre in range(len(self.Kernel.representing_set_descriptors[iddes])):
                    # print(self.Kernel.representing_set_elements[0].shape)
                    if el == self.Kernel.representing_set_elements[iddes][countRepre]:
                        repres_descs_temp[el].append(self.Kernel.representing_set_descriptors[iddes][countRepre])
                    else:
                        repres_descs_temp[el].append(np.zeros(ldesc))
                repres_descs_temp[el] = np.array(repres_descs_temp[el])
            self.Kernel.repres_descs.append((repres_descs_temp))

    
    def train(self, 
              targets = ["energy"], 
              target_sigmas = [1], 
            #   sigmaE =0.1, 
            #   sigmaF =0.01, 
              atom_energy = None, 
              energy_keyword = "energy",
              forces_keyword = "forces"):
        bigKmm = stack_matrices_diagonally(self.K_mms)
        for id_target in range(0,len(targets)):
            if targets[id_target] == "energy":
                for mol in self.Kernel.training_set:
                    ref = 1*get_energies_atomic(mols=self.Kernel.training_set,atom_energy = atom_energy, energy_keyword = energy_keyword)
                    Ks = np.array(self.Kernel.kernel_matrices(mol_set1 = [mol],kerneltype="predicting"))
                    LK = np.sum(Ks,axis=1)
                    LK = LK.reshape(1,-1)
                    try:
                        allKLsenergy = np.concatenate([allKLsenergy,LK],axis=0)
                    except:
                        allKLsenergy = LK#np.sum(K,axis=1)
                diag_sigmas_sqrt = np.sqrt((1/target_sigmas[id_target]))*np.ones(len(ref))
                b_up = diag_sigmas_sqrt*ref
                B_up = diag_sigmas_sqrt*allKLsenergy.T
            if targets[id_target] == "forces":
                for id_mol,mol in enumerate(self.Kernel.training_set):
                    # dimensions of self.Kernel.dKs_train N_systems,N_descriptors, N_atoms_in_system, directions, N_atoms_in_system, SOAP_features
                    current_dKdrs = self.Kernel.dKs_train[id_mol]
                    current_LK = np.einsum("abcde->abce",current_dKdrs)
                    dKdrs_reshaped = current_LK.transpose(1,2, 0,3)
                    dims = current_LK.shape
                    # current_LK = np.reshape(current_LK,(dims[1]*dims[2],dims[0]*dims[3]))
                    current_LK = np.reshape(dKdrs_reshaped,(dims[1]*dims[2],dims[0]*dims[3]))
                    # current_LK = current_LK.reshape(-1,current_dKdrs.shape[-1]) # For 1 SOAP, this will be (N_atoms_in_system*directions,SOAP_features)
                    # current_LK = current_LK.reshape(dims[) # For 1 SOAP, this will be (N_atoms_in_system*directions,SOAP_features)
                    try:
                        allKLsforces = np.concatenate((allKLsforces,current_LK))
                    except:
                        allKLsforces = current_LK
                # ref = -1*get_forces_eV(mols=self.Kernel.training_set, forces_keyword = forces_keyword).flatten()#/(Hartree/Bohr)# # eV/A
                ref = -1*get_forces_atomic(mols=self.Kernel.training_set, forces_keyword = forces_keyword).flatten()#/(Hartree/Bohr)# # eV/A
                diag_sigmas_sqrt = np.sqrt((1/target_sigmas[id_target]))*np.ones(len(ref))
                b_up_temp = diag_sigmas_sqrt*(ref)
                B_up_temp = diag_sigmas_sqrt*allKLsforces.T
                try:
                    B_up = np.concatenate((B_up,B_up_temp),axis=1)
                    b_up = np.concatenate((b_up,b_up_temp))
                except:
                    B_up = B_up_temp
                    b_up = b_up_temp
        
        M = bigKmm.shape[0]
        Umm = np.linalg.cholesky(bigKmm).T
        A = np.concatenate([B_up.T,Umm],axis=0)
        b = np.concatenate((b_up,np.zeros(bigKmm.shape[0])))
        print(A.shape,b.shape)
        Q,R = np.linalg.qr(A) #,mode="complete")
        Qb = np.dot(Q.T,b)
        weights = np.linalg.solve(R,Qb)
        self.weights = weights

    def loss_force(self,params):
        weights = params
        mols = self.Kernel.training_set
        train_forces = []
        for id_mol in range(len(mols)):
            nAt = len(mols[id_mol])
            # Ks,dKdrs  = self.Kernel._calculate_function(mol_set1 = mol) 
            # KsStack = np.hstack(Ks)
            # dKdrs = np.array(dKdrs)
            # site_energies = np.matmul(KsStack,weights)
            # energy =  np.sum(site_energies)
            # dKdrs_reshaped = dKdrs.transpose(1, 2, 3, 0, 4).squeeze()
            dKdrs_reshaped = self.dKtrain[id_mol]
            site_energies_drj = np.einsum("abcd,d->abc",dKdrs_reshaped,weights)
            f_k  = np.zeros((nAt,3))
            f_k -= np.sum(site_energies_drj, axis=2)
            forces = f_k #* Hartree/Bohr
            train_forces.extend(forces)
        train_forces = np.array(train_forces).flatten()
        ref = 1*get_forces_eV(mols=self.Kernel.training_set, forces_keyword = "forces").flatten()
        mse = np.mean((ref - train_forces) ** 2)
        return mse

    def train_minimize(self):#,sigma,X_train=None, y_train=None):
        self.dKtrain = []
        for mol in self.Kernel.training_set:
            nAt = len(mol)
            Ks,dKdrs  = self.Kernel._calculate_function(mol_set1 = mol) 
            KsStack = np.hstack(Ks)
            dKdrs = np.array(dKdrs)
            dKdrs_reshaped = dKdrs.transpose(1, 2, 3, 0, 4).squeeze()
            self.dKtrain.append(dKdrs_reshaped)
        initial_weights = self.weights# np.random.rand(self.Kernel.nAt_train)
        weights_res = minimize(self.loss_force, initial_weights, method='BFGS')
        print(len( weights_res.x))
        self.weights = weights_res.x

    def create_SigmaEF(self,E, F):
        n_rows = E.shape[0] + F.shape[0]
        n_cols = E.shape[1] + F.shape[1]
        SigmaEF = np.zeros((n_rows, n_cols))
        SigmaEF[:E.shape[0], :E.shape[1]] = E
        SigmaEF[E.shape[0]:, E.shape[1]:] = F
        return SigmaEF

    def calculateE(self, mol, charge=0):
        Ks = self.Kernel.kernel_matrices(mol_set1 = [mol],kerneltype="predicting")
        Ks = np.hstack(Ks)
        atomEs = np.matmul(Ks,self.weights)
        energy =  np.sum(atomEs)
        return {'energy':np.sum(energy)*Hartree}
        

    def calculate(self, mol):
        nAt = len(mol)
        Ks,dKdrs  = self.Kernel._calculate_function(mol_set1 = mol)
        Ks = np.array(Ks)
        KsStack = np.hstack(Ks)
        dKdrs = np.array(dKdrs)
        site_energies = np.matmul(KsStack,self.weights)
        energy =  np.sum(site_energies)
        dKdrs_reshaped = dKdrs.transpose(1, 2, 3, 0, 4)#.squeeze()
        dims = dKdrs_reshaped.shape
        dKdrs_reshaped = np.reshape(dKdrs_reshaped, (dims[0],dims[1],dims[2],dims[3]*dims[4])) 
        site_energies_drj = np.einsum("abcd,d->abc",dKdrs_reshaped,self.weights)
        '''
        same as
        for j in range(nAt):
            for direction in range(3):
                site_energies_drj[j][direction] = np.matmul(dKdrs_reshaped[j][direction],self.weights)
        '''
        f_k  = np.zeros((nAt,3))
        f_k -= np.sum(site_energies_drj, axis=2)
        '''
        same as
        for j in range(nAt):
            for direction in range(3):
                for i in range(nAt):
                    f_k[j,direction] += -site_energies_drj[j][direction][i]
        '''
        forces = f_k * Hartree/Bohr
        energy = energy*Hartree
        results = {'energy':energy,'forces':forces}
        return results

    def TrainEnergy(self,weights,L):
        Knm = np.hstack(self.K_nms)
        Egap = np.linalg.multi_dot([L.T,Knm,weights])
        # Egap = np.linalg.multi_dot([L,Eatoms])

        # Egap =  np.sum(Eatoms)
        return Egap



    def buildLgap(self):
        L = np.zeros((self.Kernel.nAt_train,len(self.Kernel.training_set)))
        count_c = 0
        count_r = 0
        for ids,mol in enumerate(self.Kernel.training_set):
            q_temp = []
            for q in mol: 
                L[count_r,count_c] = 1.0
                count_r += 1.0
            count_c += 1.0
        return L
