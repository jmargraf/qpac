from ase.calculators.calculator import Calculator, all_changes
from ase.calculators.calculator import Calculator


class Kqeq(Calculator):
    "Calculator for Kernel Charge Equilibration Methods (KQEq)."
    def __init__(self, kqeq, **kwargs):
        """
        Parameters
        ----------
        kqeq : kernel_qeq-object
            An instance of the kernel_qeq-class for which
            training has been performed (i.e. weights are available).
        """
        super().__init__()

        # Checks
        ## we need a functional, i.e. already trained kqeq instance
        assert kqeq.weights is not None, 'A trained kqeq is required'

        # Definitions
        self.kqeq = kqeq
        self.implemented_properties = ['energy', 'forces', 'dipole_vector', 'charges']


    def calculate(self, atoms=None, properties=['energy', 'forces', 'dipole_vector', 'charges'], system_changes=all_changes):
        "Calculates properties for given Atoms-object and fills self.results."
        # basic Calculator class asks us to call its calculate-function before our implementation
        # (will set the atoms attribute)
        super().calculate(atoms, properties, system_changes)

        self.results.update(self.kqeq.calculate(atoms))

    def get_dipole_vector(self, atoms=None):
        "Return value of property ``dipole_vector`` (which is not a standard ASE property)."
        return self.get_property('dipole_vector', atoms)


class gap_calculator(Calculator):
    def __init__(self, gap,ext_field,total_charge, **kwargs):
        super().__init__()

        # Checks
        ## we need a functional, i.e. already trained kqeq instance
        assert gap.weights is not None, 'A trained GAP is required'

        # Definitions
        self.gap = gap 
        self.implemented_properties = ['energy', 'forces']
        self.ext_field = ext_field
        self.total_charge = total_charge

    def calculate(self, atoms=None, properties=['energy', 'forces'], system_changes=all_changes):
        "Calculates properties for given Atoms-object and fills self.results."
        # basic Calculator class asks us to call its calculate-function before our implementation
        # (will set the atoms attribute)
        super().calculate(atoms, properties, system_changes)

        self.results.update(self.gap.calculate(atoms,))

        
class ML_calculator(Calculator):
    def __init__(self, gap,total_charge, **kwargs):
        super().__init__()

        # Checks
        ## we need a functional, i.e. already trained kqeq instance
        assert gap.weights is not None, 'A trained GAP is required'

        # Definitions
        self.ML_model = ML_model
        self.implemented_properties = ['energy', 'forces']
        # self.ext_field = ext_field
        self.total_charge = total_charge

    def calculate(self, atoms=None, properties=['energy', 'forces'], system_changes=all_changes):
        "Calculates properties for given Atoms-object and fills self.results."
        # basic Calculator class asks us to call its calculate-function before our implementation
        # (will set the atoms attribute)
        super().calculate(atoms, properties, system_changes)

        self.results.update(self.ML_model.calculate(atoms,total_charge=self.total_charge))
