import numpy as np
from scipy.special import erf,erfc
import sys
np.set_printoptions(threshold=sys.maxsize)
positions = np.array([[0, 0, 0],[1, 1, 0],[1, 0, 1], [0, 1, 1], [1, 1, 1], [1, 0, 0],[0, 1, 0],[0, 0, 1]])
lat = np.array([[2,0,0],[0,2,0],[0,0,2]])
unit = np.array([[1,0,0],[0,1,0],[0,0,1]])
q = [1,1,1,1,-1,-1,-1,-1]
# sigma = [-1,0]
sigma = np.array([0.3,0.1,0.2,0.7,0.4,0.1,0.1,0.05,0.03])
hard = np.array([1.2,0.3,0.2,0.8,0.2,0.5,0.12,0.78])
nat = 8
ewaldSummationPrecision = 1E-8


def dAxyzQRecip(nat,ats,reclat,n,eta,cutoff,V,q):
    dArecip = np.zeros((nat,nat,3))
    cutoff = getOptimalCutoffRecip(eta,ewaldSummationPrecision)
    n = getNcells(reclat,cutoff)
    for i in range(-n[0],n[0]+1):
        for j in range(-n[1],n[1]+1):
            for k in range(-n[2],n[2]+1):
                if (i != 0 or j != 0 or k!=0):
                    dlat = i*reclat[0] + j*reclat[1] + k*reclat[2]
                    r = sum(dlat**2)
                    if r > cutoff**2:
                        continue
                    factor = np.exp(-eta**2*r/2)/r
                    # print(factor)
                    for iat in range(nat):
                        kri = sum(dlat*ats[iat])
                        dkri = dlat.copy()
                        # print(kri)
                        for jat in range(iat,nat):
                            krj = sum(dlat*ats[jat])
                            dkrj = dlat.copy()
                            dAdxi = factor * (-np.sin(kri) * dkri * np.cos(krj) + np.cos(kri) * dkri * np.sin(krj))
                            dArecip[iat,iat] += dAdxi * q[jat]
                            dArecip[jat,iat] += dAdxi * q[iat]
                            dArecip[jat,jat] -= dAdxi * q[iat]
                            dArecip[iat,jat] -= dAdxi * q[jat]
    # dArecip = dArecip/V*4*np.pi/2
    return dArecip


def dAxyzQReal(nat,ats,lat,sigma,q,eta,n,cutoff):
    dAreal = np.zeros((nat,nat,3))
    invsqrt2eta = 1 / (np.sqrt(2) * eta)
    inv2eta = 1 / (2* eta)
    for iat in range(nat):
        for i in range(-n[0],n[0]+1):
            for j in range(-n[1],n[1]+1):
                for k in range(-n[2],n[2]+1):
                    dlat = i*lat[0] + j*lat[1] + k*lat[2]
                    for jat in range(iat,nat):
                        if (i != 0 or j != 0 or k!=0 or iat!=jat):
                            d = ats[iat] - ats[jat] + dlat
                            d2 = np.sum(d**2)
                            if d2 > cutoff**2:
                                continue
                            r = np.sqrt(d2)
                            interf = erfc(r*invsqrt2eta)
                            if sigma[0] > 0:
                                gamma = np.sqrt(sigma[iat]**2+sigma[jat]**2)
                                interf = interf - erfc(r/(np.sqrt(2)*gamma))
                            f = d/r**3*(2*r*invsqrt2eta* np.exp(-(invsqrt2eta**2)*r**2) /np.sqrt(np.pi)- 2* r/(np.sqrt(2)*gamma)*np.exp(-1/(2*gamma**2)*r**2)/np.sqrt(np.pi)+interf)/2
                            dAreal[iat,iat] = dAreal[iat,iat] - f*q[jat]
                            dAreal[jat,jat] = dAreal[jat,jat] + f*q[iat]
                            dAreal[jat,iat] = dAreal[jat,iat] - f*q[iat]
                            dAreal[iat,jat] = dAreal[iat,jat] + f*q[jat]
    return dAreal


def getOptimalEtaMatrx(nat,lat):
    return 1/np.sqrt(2*np.pi)*np.linalg.det(lat)**(1/3)

def getOptimalCutoffReal(eta,A):
    r = np.sqrt(2)*eta*np.sqrt(-np.log(A))
    return r

def getOptimalCutoffRecip(eta,A):
    r = np.sqrt(2)/eta*np.sqrt(-np.log(A))
    return r

def recLattice(A):
    B = np.linalg.inv(A)*(2*np.pi)
    return B

def getNcells(lat,cutoff):
    proj = np.zeros(3)
    # axb = np.cross(lat[:,0],lat[:,1])
    axb = np.cross(lat[0],lat[1])
    axb = axb / np.sqrt(np.sum(axb**2))
    # axc = np.cross(lat[:,0], lat[:,2])
    axc = np.cross(lat[0], lat[2])
    axc = axc / np.sqrt(np.sum(axc**2))
    # bxc = np.cross(lat[:,1], lat[:,2])
    bxc = np.cross(lat[1], lat[2])
    bxc = bxc / np.sqrt(np.sum(bxc**2))
    proj[0] = np.dot(lat[0], bxc)
    proj[1] = np.dot(lat[1], axc)
    proj[2] = np.dot(lat[2], axb)
    n = np.ceil(cutoff/abs(proj))
    n = [int(a) for a in n]
    return n 

def eemMatrixEwald(nat,ats,lat,sigma,hard):
    sigma = np.array(sigma)
    A = np.zeros((nat,nat))
    hard = np.array(hard)#np.ones(nat)
    eta = getOptimalEtaMatrx(nat,lat)
    eta = np.max([eta,np.max(sigma)])
    Areal = AmatrixReal(nat,ats,lat,sigma,hard,eta)
    Arecip = AmatrixRecip(nat,ats,lat,eta)
    Aself = AmatrixSelf(nat,eta)
    A = Areal+ Arecip + Aself
    return A

def AmatrixSelf(nat,eta):
    A = np.zeros((nat,nat))
    for i in range(nat):
        A[i,i] = -2/(np.sqrt(2*np.pi)*eta)
    return A
    
def AmatrixRecip(nat,ats,lat,eta):
    reclat = recLattice(lat)
    V = np.linalg.det(lat)
    print("V",V)
    A = np.zeros((nat,nat))
    cutoff = getOptimalCutoffRecip(eta,ewaldSummationPrecision)
    n = getNcells(reclat,cutoff)
    print(n)
    for i in range(-n[0],n[0]+1):
        for j in range(-n[1],n[1]+1):
            for k in range(-n[2],n[2]+1):
                if (i != 0 or j != 0 or k!=0):
                    dlat = i*reclat[0] + j*reclat[1] + k*reclat[2]
                    r = sum(dlat**2)
                    if r > cutoff**2:
                        continue
                    factor = np.exp(-eta**2*r/2)/r
                    # print(factor)
                    for iat in range(nat):
                        kri = sum(dlat*ats[iat])
                        # print(kri)
                        for jat in range(iat,nat):
                            krj = sum(dlat*ats[jat])
                            # print("Hej ",factor, kri,krj,np.cos(kri), "Ha")
                            # print(factor * (np.cos(kri) * np.cos(krj) + np.sin(kri)*np.sin(krj)))
                            A[iat,jat] = A[iat,jat] +  factor * (np.cos(kri) * np.cos(krj) + np.sin(kri)*np.sin(krj))
    for iat in range(nat):
        for jat in range(iat+1,nat):
            A[jat,iat] = A[iat,jat]
    A = A/V *4*np.pi
    return A
                        
def AmatrixReal(nat,ats,lat,sigma,hard,eta):
    A = np.zeros((nat,nat))
    invsqrt2eta = 1 / (np.sqrt(2) * eta)
    inv2eta = 1 / (2* eta)
    cutoff = getOptimalCutoffReal(eta,ewaldSummationPrecision)
    n = getNcells(lat,cutoff)
    print(eta,cutoff,n)
    # count = 0
    for iat in range(nat):
        for i in range(-n[0],n[0]+1):
            for j in range(-n[1],n[1]+1):
                for k in range(-n[2],n[2]+1):
                    dlat = i*lat[0] + j*lat[1] + k*lat[2]
                    for jat in range(iat,nat):
                        if (i != 0 or j != 0 or k!=0 or iat!=jat):
                            d = ats[iat] - ats[jat] + dlat
                            # print(d)
                            d2 = np.sum(d**2)
                            # print(d2)
                            # print(d2)
                            if d2 > cutoff**2:
                                continue
                            # print("hey")
                            r = np.sqrt(d2)
                            interf = erfc(r*invsqrt2eta)
                            # print(interf)
                            if sigma[0] > 0:
                                # print(sigma[iat],iat,jat)
                                gamma = np.sqrt(sigma[iat]**2+sigma[jat]**2)
                                interf = interf - erfc(r/(np.sqrt(2)*gamma))
                            A[iat,jat] = A[iat,jat] + interf/r
                            # print(count)
                            # count += 1
        if (sigma[0] > 0):
            # print(A[iat,iat],sigma[iat],hard[iat])
            A[iat,iat] = A[iat,iat] + 1/(sigma[iat]*np.sqrt(np.pi)) + hard[iat]

    for iat in range(nat):
        for jat in range(iat+1,nat):
            A[jat,iat] = A[iat,jat]
    return A