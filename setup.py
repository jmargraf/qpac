import sys
from distutils.ccompiler import new_compiler
from distutils.sysconfig import customize_compiler
from setuptools import setup, find_packages, Extension
import subprocess
from pybind11.setup_helpers import Pybind11Extension, build_ext

cpp_extra_link_args = [
"-lgomp"
]
cpp_extra_compile_args = [
    "-fopenmp", 
    "-Wall", 
    "-shared", 
    "-std=c++11", 
    "-O3",                          # O3 optimizations
]

extensions = [
    Pybind11Extension(
        'qpac/ewaldCPP',
        ["qpac/ewald.cpp"],
        #include_dirs=[
        #    get_pybind_include(),
        #    get_pybind_include(user=True),
        #],
        language='c++',
        extra_compile_args=cpp_extra_compile_args,# + ["-fvisibility=hidden"],  # the -fvisibility flag is needed by pybind11
        extra_link_args=cpp_extra_link_args,
    )
]


setup(
    name='qpac',
    version='2.1',
    packages=['qpac'],
    url='https://jmargraf.gitlab.io/kqeq/',
    license='The software is licensed under the MIT License',
    author='Johannes Margraf, Martin Vondrak',
    author_email='margraf@fhi.mpg.de',
    description='A Python implementation of the kernel Charge Equilibration method, allows training and using physics-based machine-learning models for predicting charge distributions in molecules and materials.',
    install_requires=["pybind11>=2.2",'numpy','scipy','ase','dscribe'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",],
cmdclass={"build_ext": build_ext},
    ext_modules = extensions,
)
