## q-pac 

`q-pac` is a Python package mainly focused on the kernel Charge Equilibration method (more [here](https://pubs.aip.org/aip/jcp/article/159/5/054109/2905195/q-pac-A-Python-package-for-machine-learned-charge) ). This allows training and using physics-based machine-learning models for predicting charge distributions in molecules and materials.

Detailed tutorials are provided [here](https://jmargraf.gitlab.io/kqeq), or in `doc` folder.

Real examples for dipole prediction on QM9 database, and the creation of interatomic potentials for ZnO nanoparticles and ZnO bulk structures are provided in the `examples` folder.

# Installation

This package has been tested with python 3.9. External dependencies are [numpy](https://numpy.org) (for linear algebra), [ase](https://wiki.fysik.dtu.dk/ase/) (for handling structural data) and [DScribe](https://singroup.github.io/dscribe/latest/) (for calculating atomic environment representations). The `q-pac` package can be installed using ``python setup.py install``


